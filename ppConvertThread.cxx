#include "ppConvertThread.h"

#include "vtkPTSReader.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkCommand.h"

#include <QFileInfo>
#include <QInputDialog>
#include <QGroupBox>

#include "vtkSmartPointer.h"
#include "vtkPolyData.h"

#include "vtkHierarchicalBinningFilter.h"
#include "vtkPointCloudToMultiBlock.h"
#include "vtkXMLMultiBlockDataWriter.h"
#include "vtkMultiBlockToCompressedRange.h"
#include "vtkMultiBlockDataSet.h"

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

ppConvertThread::ppConvertThread()
{
  this->Binner = 0;
  this->CompressRange = 0;
  this->CloudToMultiBlock = 0;
  this->MultiBlockWriter = 0;
}

ppConvertThread::~ppConvertThread()
{
  for (size_t i = 0; i < this->InputData.size(); i++)
    {
    if (this->InputData[i])
      {
      this->InputData[i]->Delete();
      this->InputData[i] = 0;
      }
    }
  if (this->Binner)
    {
    this->Binner->Delete();
    this->Binner = 0;
    }
  if (this->CloudToMultiBlock)
    {
    this->CloudToMultiBlock->Delete();
    this->CloudToMultiBlock = 0;
    }
  if (this->MultiBlockWriter)
    {
    this->MultiBlockWriter->Delete();
    this->MultiBlockWriter = 0;
    }
  if (this->CompressRange)
    {
    this->CompressRange->Delete();
    this->CompressRange = 0;
    }
}

void ppConvertThread::Cancel()
{
  this->Binner->SetAbortExecute(1);
  if (this->CloudToMultiBlock)
    {
    this->CloudToMultiBlock->SetAbortExecute(1);
    }
  if (this->MultiBlockWriter)
    {
    this->MultiBlockWriter->SetAbortExecute(1);
    }
  if (this->CompressRange)
    {
    this->CompressRange->SetAbortExecute(1);
    }
}

void ppConvertThread::SetInputData(std::vector<vtkPolyData *> &input)
{
  for (size_t i = 0; i < this->InputData.size(); i++)
    {
    if (this->InputData[i])
      {
      this->InputData[i]->Delete();
      this->InputData[i] = 0;
      }
    }

  this->InputData.resize(input.size());

  for (size_t i = 0; i < input.size(); i++)
    {
    // we take the reference count
    this->InputData[i] = input[i];
    }
}

void ppConvertThread::run()
{
  if (this->InputData.size() == 0)
    {
    return;
    }

  // Setup a progress callback
  VTK_CREATE(ppTaskThreadProgressCallback, myCallback);
  myCallback->Thread = this;

  // two main cases.  First case is we have a vector of input data
  // so we just write it out as is. Second case is a single data object
  // that we need to bin/randomize etc.
  if (this->InputData.size() > 1)
    {
    return;
    }

  // second case

  // bin the data
  this->Binner = vtkHierarchicalBinningFilter::New();
  this->Binner->SetInputData(this->InputData[0]);
  this->Binner->AutomaticOff();
  this->Binner->SetDivisions(2,2,2);
  this->Binner->AddObserver(vtkCommand::ProgressEvent,myCallback);
  this->Binner->SetBounds(this->InputData[0]->GetBounds());
  myCallback->Start = 0.0;
  myCallback->Scale = 30.0;
  this->Binner->Update();

  // convert to multiblock
  this->CloudToMultiBlock = vtkPointCloudToMultiBlock::New();
  this->CloudToMultiBlock->SetInputConnection(this->Binner->GetOutputPort());
  myCallback->Start = 30.0;
  myCallback->Scale = 15.0;
  this->CloudToMultiBlock->AddObserver(vtkCommand::ProgressEvent,myCallback);
  this->CloudToMultiBlock->Update();
  this->Binner->GetOutput()->ReleaseData();

  // optionally optimize
  this->MultiBlockWriter = vtkXMLMultiBlockDataWriter::New();
  if (this->OutputFileName.indexOf(".vtmopc") != -1)
    {
    myCallback->Start = 45.0;
    myCallback->Scale = 15.0;
    this->CompressRange = vtkMultiBlockToCompressedRange::New();
    this->CompressRange->SetInputConnection(this->CloudToMultiBlock->GetOutputPort());
    this->CompressRange->AddObserver(vtkCommand::ProgressEvent,myCallback);
    this->CompressRange->Update();
    this->MultiBlockWriter->SetInputConnection(this->CompressRange->GetOutputPort());
    this->CloudToMultiBlock->GetOutput()->ReleaseData();
    myCallback->Start = 60.0;
    myCallback->Scale = 40.0;
    }
  else
    {
    this->MultiBlockWriter->SetInputConnection(this->CloudToMultiBlock->GetOutputPort());
    myCallback->Start = 45.0;
    myCallback->Scale = 55.0;
    }

  this->MultiBlockWriter->SetFileName(this->OutputFileName.toLatin1().constData());
  this->MultiBlockWriter->SetDataModeToAppended();
  this->MultiBlockWriter->SetHeaderTypeToUInt64();
  this->MultiBlockWriter->SetCompressorTypeToZLib();
  this->MultiBlockWriter->EncodeAppendedDataOff();
  this->MultiBlockWriter->AddObserver(vtkCommand::ProgressEvent,myCallback);
  this->MultiBlockWriter->Write();
}

void ppConvertThread::slotSetNewDataPiece(
  int pieceNum,
  vtkPolyData *pd)
{
  if (pieceNum >= this->InputData.size())
    {
    this->InputData.resize(pieceNum+1);
    }
  else
    {
    if (this->InputData[pieceNum])
      {
      this->InputData[pieceNum]->Delete();
      this->InputData[pieceNum] = 0;
      }
    }

  this->InputData[pieceNum] = pd;
}

void ppConvertThread::slotDataLoaded()
{
  this->ppTaskThread::start();
}

void ppConvertThread::start()
{
  this->ppTaskThread::start();
}
