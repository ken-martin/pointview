#include "ppMaskThread.h"

#include "vtkMaskPoints.h"
#include "vtkCommand.h"

#include "vtkSmartPointer.h"
#include "vtkPolyData.h"
#include "vtkLODPointCloudMapper.h"

#include <QInputDialog>
#include <QGroupBox>

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

ppMaskThread::ppMaskThread()
{
  this->Mask = vtkMaskPoints::New();
}

ppMaskThread::~ppMaskThread()
{
  this->Mask->Delete();
  this->Mask = 0;
  for (size_t i = 0; i < this->InputData.size(); i++)
    {
    if (this->InputData[i])
      {
      this->InputData[i]->Delete();
      this->InputData[i] = 0;
      }
    }
}

void ppMaskThread::Cancel()
{
  this->Mask->SetAbortExecute(1);
}

void ppMaskThread::SetInputData(std::vector<vtkPolyData *> &input)
{
  for (size_t i = 0; i < this->InputData.size(); i++)
    {
    if (this->InputData[i])
      {
      this->InputData[i]->Delete();
      this->InputData[i] = 0;
      }
    }

  this->InputData.resize(input.size());

  for (size_t i = 0; i < input.size(); i++)
    {
    // we take the reference count
    this->InputData[i] = input[i];
    }
}

void ppMaskThread::run()
{
  // Setup a progress callback
  VTK_CREATE(ppTaskThreadProgressCallback, myCallback);
  myCallback->Thread = this;

  this->Mask->AddObserver(vtkCommand::ProgressEvent,myCallback);

  myCallback->Scale = 100.0/this->InputData.size();
  for (size_t i = 0;
      i < this->InputData.size() &&  !this->Task->GetCancelRequested();
      i++)
    {
    vtkPolyData *pd = this->InputData[i];
    myCallback->Start = i*100.0/this->InputData.size();
    if (pd->GetPoints()->GetNumberOfPoints() > 100.0/this->Limit)
      {
      this->Mask->SetMaximumNumberOfPoints(
        (this->Limit/100.0)*pd->GetPoints()->GetNumberOfPoints());
      this->Mask->SetInputData(pd);
      this->Mask->Update();
      vtkPolyData *newPd = vtkPolyData::New();
      newPd->ShallowCopy(this->Mask->GetOutput());
      this->Mask->GetOutput()->ReleaseData();
      if (this->Task->GetCancelRequested())
        {
        newPd->Delete();
        }
      else
        {
        emit this->signalNewDataPiece(static_cast<int>(i),newPd);
        }
      }
    else // just pass empty data through
      {
      pd->Register(0);
      emit this->signalNewDataPiece(static_cast<int>(i),pd);
      }
    }
}

void ppMaskThread::start()
{
  bool ok = false;
  this->Limit =
    QInputDialog::getInt(this->Task->GetGroup(),
      "Mask Ratio",
       "What maximum percentage of points do you wish to pass through.\n"
       "Note that this algorithm may produce less than the maximum.",
       50, 1, 100, 1,
       &ok);
  if (!ok)
    {
		this->Task->SetCancelRequested(true);
    }

  this->Mask->RandomModeOn();
  this->Mask->SetRandomModeType(0);
  this->Mask->SetOnRatio(1);

  this->ppTaskThread::start();
}
