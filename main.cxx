#include <QApplication>
#if QT_VERSION < 0x050000
  #include <QCleanlooksStyle>
#endif

#include "PointView.h"

#include "QVTKOpenGLNativeWidget.h"

extern int qInitResources_icons();

int main( int argc, char** argv )
{
  // before initializing QApplication, set the default surface format.
  QSurfaceFormat::setDefaultFormat(QVTKOpenGLNativeWidget::defaultFormat());

  // QT Stuff
  QApplication app( argc, argv );

  #if QT_VERSION >= 0x050000
    QApplication::setStyle("fusion");
  #else
    QApplication::setStyle(new QCleanlooksStyle);
  #endif

  qInitResources_icons();

  PointView myPointView;
  myPointView.show();


  QObject::connect(
    &app, SIGNAL(aboutToQuit()),
    &myPointView, SLOT(aboutToQuit()));

  return app.exec();
}
