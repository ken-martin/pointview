#ifndef ppTaskThread_H
#define ppTaskThread_H

#include <QThread>
#include "vtkCommand.h"

class ppGenericTask;

class ppTaskThread : public QThread
{
  Q_OBJECT

public:
  ppTaskThread();

  void SetTask(ppGenericTask *val);
  ppGenericTask *GetTask() { return this->Task;}

  virtual void UpdateProgress(int val);
  virtual void Cancel() = 0;
  virtual void start();

signals:
    void sendProgress(int val);

protected:
  ppGenericTask *Task;
};

class ppTaskThreadProgressCallback : public vtkCommand
{
public:
  static ppTaskThreadProgressCallback *New()
    { return new ppTaskThreadProgressCallback; }
  virtual void Execute(vtkObject *, unsigned long, void *cbo)
    {
    double *prog = reinterpret_cast<double*>(cbo);
    this->Thread->UpdateProgress(
      static_cast<int>(*prog * this->Scale + this->Start));
    }
  ppTaskThread *Thread;
  double Start = 0.0;
  double Scale = 100.0;
  ppTaskThreadProgressCallback() {}
};

#endif
