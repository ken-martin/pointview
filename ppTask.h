#ifndef ppTask_H
#define ppTask_H

#include <QObject>
#include <QProgressBar>
class QLabel;
class QGroupBox;
class QPushButton;

class ppTaskGUI : public QObject
{
  Q_OBJECT

public:
  ppTaskGUI(QWidget *parent);
  ~ppTaskGUI();

  QLabel  *GetLabel() { return this->Label; }
  QProgressBar *GetBar() { return this->Bar; }
  QGroupBox *GetGroup() { return this->Group; }

  bool GetCancelRequested() {
    return this->CancelRequested; }
  void SetCancelRequested(bool v) {
    this->CancelRequested = v; }

public slots:
  virtual void Cancel();

protected:
  QProgressBar *Bar;
  QLabel *Label;
  QGroupBox *Group;
  QPushButton *CancelButton;
  bool CancelRequested;
};

class ppGenericTask
{
public:
  ppGenericTask(QWidget *parent);
  virtual ~ppGenericTask();

  // GUI access methods
  void SetLabel(const char *val);
  bool GetCancelRequested();
  void SetCancelRequested(bool v);
  QGroupBox *GetGroup();

  // start the task
  virtual void Start() = 0;

  virtual QThread *GetGenericThread() = 0;

protected:
  ppTaskGUI *GUI;
};

template <class T> class ppTask : public ppGenericTask
{
public:
  ppTask(QWidget *parent);
  virtual ~ppTask();

  T *GetThread() { return this->Thread; }

  virtual QThread *GetGenericThread() {
    return this->Thread; }

  // start the task
  virtual void Start();

protected:
  T *Thread;
};

template <class T>
ppTask<T>::ppTask(QWidget *parent) : ppGenericTask(parent)
{
  this->Thread = new T();
  this->Thread->SetTask(this);
  QObject::connect(
    this->Thread, SIGNAL(sendProgress(int)),
    this->GUI->GetBar(), SLOT(setValue(int)),
    Qt::QueuedConnection);
}

template <class T>
ppTask<T>::~ppTask()
{
  if (this->Thread)
    {
    delete this->Thread;
    this->Thread = 0;
    }
}

template <class T>
void ppTask<T>::Start()
{
  this->Thread->start();
}

#endif // ppTask_H
