#ifndef ppReadThread_H
#define ppReadThread_H

#include "ppTask.h"
#include "ppTaskThread.h"
#include "vtkNew.h"
#include "vtkPLYReader.h"

class vtkAlgorithm;
class vtkPolyData;

class ppReadThread : public ppTaskThread
{
  Q_OBJECT

public:
  ppReadThread();
  ~ppReadThread();

  virtual void start();
  virtual void Cancel();

  void SetSendCompressed(bool v) {
    this->SendCompressed = v; }

  // prompts the user to
  // determine what file to read
  bool DetermineFileName(QWidget *parent);

  // returns true if the file can be read
  void SetFileName(QString val)
  { this->FileName = val; }

  static bool CanReadFile(const char *fileName);

  static const char *GetSupportFormats()
    {
		return
			"All Formats (*.vtmopc *.vtm *.vtp *.pts *.ply);;Compressed PointView Files (*.vtmopc);;VTK MultiBlock Files (*.vtm);;VTK PolyData Files (*.vtp);;PTS Data (*.pts);;PLY Files (*.ply)";
    }

signals:
  void signalNewDataPiece(
    int piece,
    vtkPolyData *output);

private:
  void run();
  vtkAlgorithm *Reader;
  QString FileName;
  bool SendCompressed;
};

#endif // ppReadThread_H
