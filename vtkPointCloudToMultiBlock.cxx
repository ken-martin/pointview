/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPointCloudToMultiBlock.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPointCloudToMultiBlock.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkNew.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkDoubleArray.h"

vtkStandardNewMacro(vtkPointCloudToMultiBlock);

//-----------------------------------------------------------------------------
vtkPointCloudToMultiBlock::vtkPointCloudToMultiBlock()
{
  this->ModuloOrdering = true;
}

//----------------------------------------------------------------------------
int vtkPointCloudToMultiBlock::FillInputPortInformation(int, vtkInformation *info)
{
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
  return 1;
}

//-----------------------------------------------------------------------------
int vtkPointCloudToMultiBlock::RequestData(
  vtkInformation *vtkNotUsed(request),
  vtkInformationVector **inputVector,
  vtkInformationVector *outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and output
  vtkPolyData *input = vtkPolyData::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkMultiBlockDataSet *output =
    vtkMultiBlockDataSet::SafeDownCast(
      outInfo->Get(vtkDataObject::DATA_OBJECT()));

  // handle input data
  vtkPointData *inPD = input->GetPointData();
  vtkFieldData *inFD = input->GetFieldData();
  vtkDataArray *offsets = inFD->GetArray("BinOffsets");

  // for each piece create a polydata and put it in the
  // multiblock dataset
  int numPieces = offsets->GetNumberOfTuples()-1;
  output->SetNumberOfBlocks(numPieces);

  for (int piece = 0; piece < numPieces && !this->AbortExecute; piece++)
    {
    vtkNew<vtkPolyData> pd;

    vtkIdType startIndex, endIndex;
    if (vtkIntArray::SafeDownCast(offsets))
      {
      vtkIntArray *ioffs = vtkIntArray::SafeDownCast(offsets);
      startIndex = ioffs->GetValue(piece);
      endIndex = ioffs->GetValue(piece+1);
      }
    else
      {
      vtkIdTypeArray *ioffs = vtkIdTypeArray::SafeDownCast(offsets);
      startIndex = ioffs->GetValue(piece);
      endIndex = ioffs->GetValue(piece+1);
      }

    vtkIdType numPts = endIndex - startIndex;
    vtkPointData *outPD = pd->GetPointData();
    outPD->CopyAllocate(inPD,numPts);

    vtkNew<vtkPoints> newPoints;
    newPoints->Allocate(numPts);

    newPoints->SetNumberOfPoints(numPts);

    if (this->ModuloOrdering)
      {
      // loop over points copying them to the output
      // we do it in an mod 11 approach to add some randomization to the order
      vtkIdType inIdx = 0;
      vtkIdType nextStart = 1;
      for (vtkIdType i = 0; i < numPts; i++)
        {
        newPoints->SetPoint(i,input->GetPoint(inIdx+startIndex));
        outPD->CopyData(inPD, inIdx + startIndex, i);
        inIdx += 11;
        if (inIdx >= numPts)
          {
          inIdx = nextStart;
          nextStart++;
          }
        }
      }
    else // otherwise no reordering
      {
      // copy the points
      newPoints->InsertPoints(0, numPts, startIndex, input->GetPoints());
      // copy point data
      outPD->CopyData(inPD, 0, numPts, startIndex);
      }
    pd->SetPoints(newPoints.Get());

    output->SetBlock(piece,pd.Get());
    this->UpdateProgress(static_cast<float>(piece)/numPieces);
    }

  return 1;
}

//-----------------------------------------------------------------------------
void vtkPointCloudToMultiBlock::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "ModuloOrdering: " << this->ModuloOrdering << "\n";
}
