#ifndef ppMaskThread_H
#define ppMaskThread_H

#include "ppTask.h"
#include "ppTaskThread.h"

class vtkPolyData;
class vtkMaskPoints;
class vtkUnsignedCharArray;

class ppMaskThread : public ppTaskThread
{
  Q_OBJECT

public:
  ppMaskThread();
  ~ppMaskThread();

  virtual void start();
  virtual void Cancel();

  void SetInputData(std::vector<vtkPolyData *> &data);

signals:
    void signalNewDataPiece(
      int piece,
      vtkPolyData *output);

private:
  void run();
  vtkMaskPoints *Mask;
  std::vector<vtkPolyData *> InputData;
  int Limit;
};

#endif // ppMaskThread_H
