#ifndef ppConvertThread_H
#define ppConvertThread_H

#include "ppTask.h"
#include "ppTaskThread.h"

class vtkAlgorithm;
class vtkPolyData;
class vtkHierarchicalBinningFilter;
class vtkUnsignedCharArray;
class vtkPointCloudToMultiBlock;
class vtkXMLMultiBlockDataWriter;
class vtkMultiBlockToCompressedRange;

class ppConvertThread : public ppTaskThread
{
  Q_OBJECT

public:
  ppConvertThread();
  ~ppConvertThread();

  virtual void start();
  virtual void Cancel();

  void SetOutputFileName(QString val)
  { this->OutputFileName = val; }

  void SetInputData(std::vector<vtkPolyData *> &data);

public slots:
  virtual void slotSetNewDataPiece(int piece, vtkPolyData *);
  virtual void slotDataLoaded();

signals:
    void signalNewDataPiece(
      int piece,
      vtkPolyData *output);

private:
  void run();
  vtkHierarchicalBinningFilter *Binner;
  QString OutputFileName;

  std::vector<vtkPolyData *> InputData;

  vtkPointCloudToMultiBlock *CloudToMultiBlock;
  vtkXMLMultiBlockDataWriter *MultiBlockWriter;
  vtkMultiBlockToCompressedRange *CompressRange;
};


#endif // ppConvertThread_H
