#include "ppReadThread.h"

#include "vtkPTSReader.h"
#include "vtkXMLPolyDataReader.h"

#include <QFileInfo>
#include <QInputDialog>
#include <QGroupBox>

#include "vtkSmartPointer.h"
#include "vtkPolyData.h"
#include "vtkInformation.h"
#include "vtkStreamingDemandDrivenPipeline.h"
#include "vtkPointData.h"
#include "vtkLookupTable.h"
#include "vtkXMLMultiBlockDataReader.h"
#include "vtkCompositeDataPipeline.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkMultiBlockToCompressedRange.h"

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

void ppReadThread::Cancel()
{
  this->Reader->SetAbortExecute(1);
}

void ppReadThread::run()
{
  // Setup a progress callback
  VTK_CREATE(ppTaskThreadProgressCallback, myCallback);
  myCallback->Thread = this;
  this->Reader->AddObserver(vtkCommand::ProgressEvent,myCallback);
  vtkNew<vtkLookupTable> lut;

  vtkXMLMultiBlockDataReader *mbxml =
    vtkXMLMultiBlockDataReader::SafeDownCast(this->Reader);
  if (mbxml)
    {
    mbxml->UpdateInformation(); // invokes read xml information which sets num pieces

    vtkInformation *rdrInfo = mbxml->GetOutputInformation(0);
  	int numPieces = 1;
    if (rdrInfo->Has(vtkCompositeDataPipeline::COMPOSITE_DATA_META_DATA() ) )
      {
      vtkMultiBlockDataSet * meta =
        vtkMultiBlockDataSet::SafeDownCast(
          rdrInfo->Get(
            vtkCompositeDataPipeline::COMPOSITE_DATA_META_DATA()));
      numPieces = meta->GetNumberOfBlocks();
      }
    myCallback->Scale = 100.0/numPieces;
    for (int i = 0; i < numPieces && !this->Task->GetCancelRequested(); i++)
      {
      myCallback->Start = i*100.0/numPieces;
      vtkInformation *outInfo = mbxml->GetOutputInformation(0);
      outInfo->Set(
        vtkStreamingDemandDrivenPipeline::UPDATE_PIECE_NUMBER(), i);
      outInfo->Set(
        vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_PIECES(), numPieces);
      outInfo->Set(
        vtkStreamingDemandDrivenPipeline::UPDATE_NUMBER_OF_GHOST_LEVELS(), 0);
      mbxml->Update();

      if (!this->Task->GetCancelRequested())
        {
        vtkMultiBlockDataSet *mbdata =
          vtkMultiBlockDataSet::SafeDownCast(mbxml->GetOutput());
        // find the block
        int bcount = numPieces - 1;
        while (bcount > i && !mbdata->GetBlock(bcount))
        {
          bcount--;
        }
        if (vtkPointSet::SafeDownCast(mbdata->GetBlock(bcount)) &&
            vtkPointSet::SafeDownCast(mbdata->GetBlock(bcount))->GetPoints())
          {
          vtkPolyData *newPd = vtkPolyData::New();
          newPd->ShallowCopy(mbdata->GetBlock(bcount));
          mbxml->GetOutput()->ReleaseData();

          // if it is not compressed then compress it now
          if (this->SendCompressed &&
              !newPd->GetFieldData()->GetArray("PointCompressionFactors"))
            {
            vtkDataObject *dobj = vtkMultiBlockToCompressedRange::Compress(newPd);
            newPd->Delete();
            newPd = vtkPolyData::SafeDownCast(dobj);
            }
          emit this->signalNewDataPiece(i,newPd);
          }
        }
      }
    return;
    }

  // generic approach for everything else
  if (this->Reader)
    {
    this->Reader->Update();
    vtkPolyData *ps = vtkPolyData::SafeDownCast(this->Reader->GetOutputDataObject(0));
    if (!ps)
      {
      return;
      }
    ps->Register(0);

    if (this->SendCompressed &&
        !ps->GetFieldData()->GetArray("PointCompressionFactors"))
      {
      vtkDataObject *dobj = vtkMultiBlockToCompressedRange::Compress(ps);
      ps->Delete();
      ps = vtkPolyData::SafeDownCast(dobj);
      }

    if (!this->Task->GetCancelRequested())
      {
      emit this->signalNewDataPiece(0,ps);
      }
    else
      {
      ps->Delete();
      }
    }
}

ppReadThread::ppReadThread()
{
  this->Reader = 0;
  this->SendCompressed = true;
}

ppReadThread::~ppReadThread()
{
  this->Reader->Delete();
  this->Reader = 0;
}

bool ppReadThread::CanReadFile(const char *fileName)
{
  vtkNew<vtkPLYReader> ply;
  if (ply->CanReadFile(fileName))
    {
    return true;
    }

  vtkNew<vtkXMLMultiBlockDataReader> mbrdr;
  if (mbrdr->CanReadFile(fileName))
    {
    return true;
    }

  vtkNew<vtkXMLPolyDataReader> pdrdr;
  if (pdrdr->CanReadFile(fileName))
    {
    return true;
    }

  QFileInfo fi(fileName);
  if (fi.suffix() == "pts")
    {
    return true;
    }

  return false;
}

void ppReadThread::start()
{
  if (this->Reader)
    {
    this->Reader->Delete();
    this->Reader = 0;
    }

  {
  vtkNew<vtkXMLMultiBlockDataReader> rdr;
  if (rdr->CanReadFile(this->FileName.toLatin1().constData()))
    {
    this->Reader = rdr.Get();
    rdr->SetFileName(this->FileName.toLatin1().constData());
    rdr->Register(0);
    }
  }

  {
  vtkNew<vtkXMLPolyDataReader> rdr;
  if (rdr->CanReadFile(this->FileName.toLatin1().constData()))
    {
    this->Reader = rdr.Get();
    rdr->SetFileName(this->FileName.toLatin1().constData());
    rdr->Register(0);
    }
  }

  {
  vtkNew<vtkPLYReader> rdr;
  if (rdr->CanReadFile(this->FileName.toLatin1().constData()))
    {
    this->Reader = rdr.Get();
    rdr->SetFileName(this->FileName.toLatin1().constData());
    rdr->Register(0);
    }
  }

  if (!this->Reader &&
      this->FileName.right(4) == ".pts")
    {
    QFileInfo fi(this->FileName);
    vtkNew<vtkPTSReader> rdr;
    rdr->CreateCellsOff();
    rdr->IncludeColorAndLuminanceOff();
    rdr->SetFileName(this->FileName.toLatin1().constData());
    this->Reader = rdr.Get();

    if (fi.size() > 100000000)
      {
      bool ok = false;
      int limit =
      QInputDialog::getInt(this->Task->GetGroup(),
        "PTS File Limit",
        "The PTS file you are loading appears to be large.\n "
        "By default we will load at most the following number\n "
        "of points (specified in millions). If you wish to load\n "
        "all the points regardless of size select cancel.",
        5, 1, 1000, 1,
        &ok);
      if (ok)
        {
        rdr->LimitToMaxNumberOfPointsOn();
        rdr->SetMaxNumberOfPoints(limit*1000000);
        }
      }
  	rdr->Register(0);
    }

  if (this->Reader)
    {
    this->ppTaskThread::start();
    }
}
