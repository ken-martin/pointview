
#include "ui_PointView.h"
#include "PointView.h"

#include <vtkNew.h>
#include <vtkPointData.h>
#include <vtkPointGaussianMapper.h>
#include <vtkPointFillPass.h>
#include <vtkCameraPass.h>
#include <vtkOpenGLRenderer.h>
#include <vtkRendererCollection.h>
#include <vtkRenderStepsPass.h>
#include <vtkRenderWindow.h>
#include <vtkTimerLog.h>
#include <vtkXMLPolyDataReader.h>
#include <vtkInteractorStyleJoystickCamera.h>
#include "vtkLODPointCloudMapper.h"
#include "vtkCullerCollection.h"
#include "vtkScalarsToColors.h"

#include <QFileDialog>
#include <QLabel>
#include <QMessageBox>
#include <QSignalMapper>
#include <QCheckBox>
#include <QRadioButton>

#include "ppMaskThread.h"
#include "ppReadThread.h"
#include "ppConvertThread.h"
#include <sstream>

#include "vtkSmartPointer.h"
#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

class vtkRenderRateCallback : public vtkCommand
{
public:
  static vtkRenderRateCallback *New()
    { return new vtkRenderRateCallback; }
  virtual void Execute(vtkObject *, unsigned long, void *cbo)
    {
    this->pp->UpdateFrameRate();
    }
  PointView *pp;
  vtkRenderRateCallback() { this->pp = NULL; }
};

// Constructor
PointView::PointView()
{
  qRegisterMetaType< std::vector<vtkUnsignedCharArray *> >( "std::vector<vtkUnsignedCharArray *>" );
  this->Timer = 0;
  this->CameraPass = 0;
  this->PointFillPass = 0;
  this->RenderRateCallback = 0;
  this->Actor = 0;

  this->ui = new Ui_PointView;
  this->ui->setupUi(this);

  // this->Mapper = vtkPointGaussianMapper::New();
  // this->Mapper->EmissiveOff();
  // this->Mapper->SetScaleFactor(0.0);

  this->LODMapper = vtkLODPointCloudMapper::New();

  // Actor in scene
  this->Actor = vtkActor::New();
//  actor->SetMapper(this->Mapper);
  this->Actor->SetMapper(this->LODMapper);

  // VTK Renderer
  this->Renderer =
    vtkOpenGLRenderer::SafeDownCast(vtkRenderer::New());
  vtkCullerCollection *cullers =
    this->Renderer->GetCullers();
  this->Renderer->RemoveCuller(cullers->GetLastItem());

  // if (!this->ui->qvtkWidget->renderWindow()->SupportsOpenGL())
  //   {
  //   QMessageBox em;
  //   em.setText("This computer appears to lack support for OpenGL 3.2.\n "
  //          "Please make sure your video drivers are up to date..\n "
  //     );
  //   em.exec();
  //   exit(-1);
  //   return;
  //   }

  // Add Actor to renderer
  this->Renderer->AddActor(this->Actor);
  this->Renderer->SetNearClippingPlaneTolerance(0.02);
  // very important to have tight depth ranges
  this->Renderer->SetClippingRangeExpansion(0.0);

  // VTK/Qt wedded
  this->ui->qvtkWidget->renderWindow()->AddRenderer(this->Renderer);
  this->ui->qvtkWidget->renderWindow()->SetMultiSamples(0);

  this->ui->qvtkWidget->renderWindow()->GetInteractor()
    ->SetDesiredUpdateRate(30.0);
  this->ui->qvtkWidget->renderWindow()->GetInteractor()
    ->SetStillUpdateRate(1.0);

  vtkNew<vtkInteractorStyleJoystickCamera> is;
  this->ui->qvtkWidget->renderWindow()->GetInteractor()->
    SetInteractorStyle(is.Get());

   // Set something up to measure framerate
  this->RenderRateCallback = vtkRenderRateCallback::New();
  this->RenderRateCallback->pp = this;
  this->Renderer->AddObserver(vtkCommand::EndEvent, this->RenderRateCallback);

  // Set up action signals and slots
  connect(
    this->ui->actionMaskPoints, SIGNAL(triggered()),
    this, SLOT(slotMaskPoints()));
  connect(
    this->ui->actionOpenFile, SIGNAL(triggered()),
    this, SLOT(slotOpenFile()));
  connect(
    this->ui->actionConvertFile, SIGNAL(triggered()),
    this, SLOT(slotConvertFile()));
  connect(
    this->ui->actionSave, SIGNAL(triggered()),
    this, SLOT(slotSaveFile()));
  connect(
    this->ui->actionExit, SIGNAL(triggered()),
    this, SLOT(slotExit()));
  connect(
    this->ui->usePointFillPass, SIGNAL(stateChanged(int)),
    this, SLOT(slotUsePointFillPassChanged(int)));

  connect(
    this->ui->MinimumAngle, SIGNAL(valueChanged(int)),
    this, SLOT(slotMinimumAngleChanged(int)));
  connect(
    this->ui->CandidateRatio, SIGNAL(valueChanged(int)),
    this, SLOT(slotCandidateRatioChanged(int)));

  this->Timer = vtkTimerLog::New();
  this->Timer->StartTimer();

  this->PointFillPass = vtkPointFillPass::New();

  this->CameraPass = vtkCameraPass::New();
  this->CameraPass->SetDelegatePass(this->PointFillPass);

  // create the basic VTK render steps
  vtkNew<vtkRenderStepsPass> rsp;
  this->PointFillPass->SetDelegatePass(rsp.Get());
};

PointView::~PointView()
{
  if (this->PointFillPass)
    {
    this->PointFillPass->ReleaseGraphicsResources(
      this->ui->qvtkWidget->renderWindow());
    this->PointFillPass->Delete();
    this->PointFillPass = 0;
    }
  if (this->CameraPass)
    {
    this->CameraPass->ReleaseGraphicsResources(
      this->ui->qvtkWidget->renderWindow());
    this->CameraPass->Delete();
    this->CameraPass = 0;
    }

//  this->Mapper->Delete();
  this->LODMapper->Delete();
  std::set<ppGenericTask *>::iterator it = this->Tasks.begin();
  for (; it != this->Tasks.end(); it++)
    {
    delete *it;
    }
  this->Tasks.clear();

  if (this->RenderRateCallback)
    {
    this->RenderRateCallback->Delete();
    this->RenderRateCallback = 0;
    }

  if (this->Timer)
    {
    this->Timer->Delete();
    this->Timer = 0;
    }
  if (this->Actor)
    {
    this->Actor->Delete();
    this->Actor = NULL;
    }
  this->Renderer->Delete();
  this->Renderer = 0;
}

// compute and report a frame rate
// sort of tricky to get a good number
// we look at the interframe interval and
// if it is fairly consistent we use it.
// otherwise we wait
void PointView::UpdateFrameRate()
{
  // use a 10 render moving average
  static double recentRates[10] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
  static int idx = 0;

  this->Timer->StopTimer();
  double rate = this->Timer->GetElapsedTime();
  if (rate == 0.0)
    {
    this->ui->renderRateText->setPlainText("FPS: unknown\nMPS: unknown");
    this->Timer->StartTimer();
    return;
    }
  recentRates[idx] = rate;

  // compute the average rate
  double reportedRate = 0.0;
  for (int i = 0; i < 10; i++)
    {
    reportedRate += recentRates[i];
    }
  reportedRate = reportedRate/10.0;
  idx = (idx+1)%10;

  // determine if the five recent rates are all
  // "close" to the average and so can be trusted
  double errorRate = 0.0;
  for (int i = 0; i < 10; i++)
    {
    errorRate += abs(reportedRate - recentRates[i]);
    }
  errorRate = errorRate/10.0;

  if (errorRate/reportedRate > 1.0)
    {
    this->ui->renderRateText->setPlainText("FPS: unknown\nMPS: unknown");
    }
  else
    {
    std::ostringstream txt;
    txt << "FPS: " << floor(1.0/reportedRate) << " frames/sec\n";

    reportedRate = this->LODMapper->GetLastPointRate();
    reportedRate /= 1000000.0;  // MPoints/sec
    txt << "MPS: " << floor(reportedRate) << " MPoints/sec";
    this->ui->renderRateText->setPlainText(txt.str().c_str());
    }

  this->Timer->StartTimer();
}

// Action to be taken upon a task completing
void PointView::slotTaskCompleted()
{
  ppTaskThread *thread =
    qobject_cast<ppTaskThread *>(QObject::sender());
  if (thread)
    {
    ppGenericTask *task = thread->GetTask();
    if (task)
      {
      std::set<ppGenericTask *>::iterator it = this->Tasks.find(task);
      if (it != this->Tasks.end())
        {
        delete task;
        this->Tasks.erase(it);
        }
      }
    }
}

// Action to be taken upon adding data
void PointView::slotNewDataPiece(
  int piece,
  vtkPolyData *pd)
{
  if (piece == 0)
  	{
		//  this->Mapper->SetInputData(pd);
		this->LODMapper->ClearPieces();
		this->MemorySize = 0;
		this->NumberOfPoints = 0;
		this->PointMemory = 0;
		this->NumberOfCells = 0;
		this->CellMemory = 0;
		this->ScalarMemory = 0;

		// clear old scalar buttons and connections
		for (int i = this->ui->scalarGroupLayout->count() - 1;
			i >= 0; i--)
      {
			QWidget *w =
				this->ui->scalarGroupLayout->itemAt(i)->widget();
			w->disconnect();
			this->ui->scalarGroupLayout->removeWidget(w);
			delete w;
      }
    }

  this->LODMapper->SetDataForPiece(piece,pd);

  if (!pd)
    {
    return;
    }

  this->MemorySize += pd->GetActualMemorySize();
  this->NumberOfPoints += pd->GetPoints()->GetNumberOfPoints();
  this->PointMemory += pd->GetPoints()->GetActualMemorySize();
  this->NumberOfCells += pd->GetVerts()->GetNumberOfCells();
  this->CellMemory += pd->GetVerts()->GetActualMemorySize();
  if (pd->GetPointData()->GetScalars())
    {
    this->ScalarMemory +=
      pd->GetPointData()->GetScalars()->GetActualMemorySize();
    }

  std::ostringstream txt;
  txt << static_cast<int>(this->MemorySize/1000)
    << " MB total size\n";
  txt << static_cast<int>(this->NumberOfPoints)
    << " Points ( "
    << static_cast<int>(this->PointMemory/1000)
    << " MB)\n";
  txt << static_cast<int>(this->NumberOfCells)
    << " Cells ( "
    << static_cast<int>(this->CellMemory/1000)
    << " MB)\n";
  txt << "Scalars ( "
    << static_cast<int>(this->ScalarMemory/1000)
    << " MB)\n";
  this->ui->textBrowser->setPlainText(txt.str().c_str());

  pd->UnRegister(NULL);

  if (piece == 0)
    {
    this->Renderer->ResetCamera();
    }

  this->ui->qvtkWidget->renderWindow()->Render();
}

// Action to be taken upon file open
void PointView::slotOpenFile()
{
  QString fileName = QFileDialog::getOpenFileName(this,
    tr("Open Data File"), "",
    tr(ppReadThread::GetSupportFormats()));
  if (fileName.isNull())
    {
    return;
    }

  if (!ppReadThread::CanReadFile(fileName.toLatin1().constData()))
    {
    QMessageBox em;
    em.setText("Bad File Format.");
    em.exec();
    return;
    }

  QFileInfo fi(fileName);
  if (fi.size() > 50000000 &&
      fi.suffix() != "vtmopc" && fi.suffix() != "vtm")
    {
    QMessageBox em;
    em.setText("The file you are loading appears to be large.\n "
           "Please use the convert option to convert it to vtm or vtkopc format\n "
           "first then load the converted file.");
    em.exec();
    return;
    }

  ppTask<ppReadThread> *task = new ppTask<ppReadThread>(this->ui->threadBox);
  ppReadThread *thread = task->GetThread();
  thread->SetFileName(fileName);

  QObject::connect(
    thread,
    SIGNAL(signalNewDataPiece(int, vtkPolyData *)),
    this,
    SLOT(slotNewDataPiece(int,vtkPolyData *)),
    Qt::QueuedConnection);

  this->Tasks.insert(task);
  std::string foo = "Reading File ";
  foo += fi.fileName().toLatin1().constData();
  task->SetLabel(foo.c_str());

  QObject::connect(
    thread, SIGNAL(finished()),
    this, SLOT(slotTaskCompleted()));
  QObject::connect(
    thread, SIGNAL(finished()),
    this, SLOT(slotNewDataComplete()));
  task->Start();
}

// Action to be taken upon file conversion
void PointView::slotConvertFile()
{
  QString fileName = QFileDialog::getOpenFileName(this,
    tr("Open Data File"), "",
    tr(ppReadThread::GetSupportFormats()));
  if (fileName.isNull())
    {
    return;
    }

  if (!ppReadThread::CanReadFile(fileName.toLatin1().constData()))
    {
    QMessageBox em;
    em.setText("Bad File Format.");
    em.exec();
    return;
    }

  QString outFileName = QFileDialog::getSaveFileName(this,
    tr("VTK Point File"), "",
    tr("Compressed PointView Data (*.vtmopc);;VTK MultiBlock (*.vtm)"));
  if (outFileName.isNull())
    {
    return;
    }

  QFileInfo fi(fileName);

  // create a read thread to read the file in
  ppTask<ppReadThread> *task = new ppTask<ppReadThread>(this->ui->threadBox);
  ppReadThread *thread = task->GetThread();
  thread->SetFileName(fileName);
  thread->SetSendCompressed(false);

  this->Tasks.insert(task);
  std::string foo = "Reading File ";
  foo += fi.fileName().toLatin1().constData();
  task->SetLabel(foo.c_str());

  // and a convert thread to convert the data
  ppTask<ppConvertThread> *ctask = new ppTask<ppConvertThread>(this->ui->threadBox);
  ppConvertThread *cthread = ctask->GetThread();
  cthread->SetOutputFileName(outFileName);

  this->Tasks.insert(ctask);
  foo = "Converting ";
  foo += fi.fileName().toLatin1().constData();
  ctask->SetLabel(foo.c_str());

  // connect the two threads
  QObject::connect(
    thread,
    SIGNAL(signalNewDataPiece(int, vtkPolyData *)),
    cthread,
    SLOT(slotSetNewDataPiece(int,vtkPolyData *)),
    Qt::QueuedConnection);

  QObject::connect(
    thread, SIGNAL(finished()),
    cthread, SLOT(slotDataLoaded()));
  QObject::connect(
    thread, SIGNAL(finished()),
    this, SLOT(slotTaskCompleted()));
  QObject::connect(
    cthread, SIGNAL(finished()),
    this, SLOT(slotTaskCompleted()));

  task->Start();
}

// Action to be taken upon file save
void PointView::slotSaveFile()
{
  QString fileName = QFileDialog::getSaveFileName(this,
    tr("VTK Point File"), "",
    tr("Compressed PointView Data (*.vtmopc);;VTK MultiBlock (*.vtm)"));
  if (fileName.isNull())
    {
    return;
    }

  QFileInfo fi(fileName);

  ppTask<ppConvertThread> *task = new ppTask<ppConvertThread>(this->ui->threadBox);
  ppConvertThread *thread = task->GetThread();
  thread->SetOutputFileName(fileName);

  // pass the data into the filter
  std::vector<vtkPolyData *> data;
  data.resize(this->LODMapper->GetNumberOfPieces());
  for (size_t i = 0; i < data.size(); i++)
    {
    data[i] = this->LODMapper->GetDataForPiece(
    static_cast<int>(i));
    data[i]->Register(NULL);
    }

  thread->SetInputData(data);

  this->Tasks.insert(task);
  std::string foo = "Saving File ";
  foo += fi.fileName().toLatin1().constData();
  task->SetLabel(foo.c_str());

  QObject::connect(
    thread, SIGNAL(finished()),
    this, SLOT(slotTaskCompleted()));
  task->Start();
}

// Action to be taken upon file open
void PointView::slotMaskPoints()
{
  ppTask<ppMaskThread> *task = new ppTask<ppMaskThread>(this->ui->threadBox);
  ppMaskThread *thread = task->GetThread();
  this->Tasks.insert(task);
  task->SetLabel("Masking Points");

  // pass the data into the filter
  std::vector<vtkPolyData *> data;
  data.resize(this->LODMapper->GetNumberOfPieces());
  for (size_t i = 0; i < data.size(); i++)
    {
    data[i] = this->LODMapper->GetDataForPiece(
		static_cast<int>(i));
    data[i]->Register(NULL);
    }

  thread->SetInputData(data);

  QObject::connect(
    thread,
    SIGNAL(signalNewDataPiece(int, vtkPolyData *)),
    this,
    SLOT(slotNewDataPiece(int,vtkPolyData *)),
    Qt::QueuedConnection);
  QObject::connect(
    thread, SIGNAL(finished()),
    this, SLOT(slotTaskCompleted()));
  QObject::connect(
    thread, SIGNAL(finished()),
    this, SLOT(slotNewDataComplete()));
  task->Start();
}

void PointView::slotUsePointFillPassChanged(int state)
{
  if (state == Qt::Unchecked)
    {
    this->Renderer->SetPass(NULL);
    this->ui->qvtkWidget->renderWindow()->Render();
    }
  if (state == Qt::Checked)
    {
    this->Renderer->SetPass(this->CameraPass);
    this->ui->qvtkWidget->renderWindow()->Render();
    }
}

void PointView::slotMinimumAngleChanged(int val)
{
  this->PointFillPass->SetMinimumCandidateAngle(val*3.1415926*2.0/99.0);
  std::ostringstream txt;
  txt << "Minimum Angle: " << floor(val*360.0/99.0);
  this->ui->MinimumAngleLabel->setText(txt.str().c_str());
  this->ui->qvtkWidget->renderWindow()->Render();
}

void PointView::slotCandidateRatioChanged(int val)
{
  this->PointFillPass->SetCandidatePointRatio(0.95 + 0.05*val/99.0);
  std::ostringstream txt;
  txt << "Canadidate Ratio: " << floor(this->PointFillPass->GetCandidatePointRatio()*1000.0)/1000;
  this->ui->CandidateRatioLabel->setText(txt.str().c_str());
  this->ui->qvtkWidget->renderWindow()->Render();
}

void PointView::slotExit()
{
  qApp->quit();
}

void PointView::aboutToQuit()
{
  // set threads to abort and wait for them
  std::set<ppGenericTask *>::iterator it = this->Tasks.begin();
  for (; it != this->Tasks.end(); it++)
    {
    (*it)->SetCancelRequested(true);
    }
  for (it = this->Tasks.begin(); it != this->Tasks.end(); it++)
    {
    (*it)->GetGenericThread()->wait();
    }
}

void PointView::scalarBoxClicked(bool val)
{
  this->LODMapper->ClearColorData();

  if (!val)
    {
    this->ui->qvtkWidget->renderWindow()->Render();
    return;
    }

  // uncheck other scalar buttons
  for (int i = this->ui->scalarGroupLayout->count() -1;
       i >= 0; i--)
    {
    QWidget *w =
      this->ui->scalarGroupLayout->itemAt(i)->widget();
    QCheckBox *cb = qobject_cast<QCheckBox *>(w);
    if (cb != QObject::sender())
      {
      cb->setChecked(false);
      }
    }

  vtkPolyData *pd = this->LODMapper->GetDataForPiece(0);
  vtkDataSetAttributes *dsa = pd->GetPointData();

  QCheckBox *box = qobject_cast<QCheckBox *>(QObject::sender());
  std::string selectedName = box->text().toLatin1().constData();
  this->LODMapper->SetNewColorData(box->text().toLatin1().constData());
  this->ui->qvtkWidget->renderWindow()->Render();
  return;
}

void PointView::slotNewDataComplete()
{
  vtkPolyData *pd = this->LODMapper->GetDataForPiece(0);
  vtkDataSetAttributes *dsa = pd->GetPointData();
  vtkDataArray *scalars = dsa->GetScalars();
  std::string selectedName;
  if (scalars && scalars->GetName())
    {
    selectedName = scalars->GetName();
    }
  for (int i = 0; i < dsa->GetNumberOfArrays(); i++)
    {
    std::string name = dsa->GetArrayName(i);
//      QRadioButton *box = new QRadioButton(tr(name.c_str()));
    QCheckBox *box = new QCheckBox(tr(name.c_str()));
    this->ui->scalarGroupLayout->addWidget(box);
    QObject::connect(
      box, SIGNAL(clicked(bool)),
      this, SLOT(scalarBoxClicked(bool)));
    if (selectedName == name)
      {
      box->setChecked(true);
      this->LODMapper->GetLookupTable()->SetRange(dsa->GetArray(i)->GetRange());
      dsa->SetActiveScalars(name.c_str());
      }
    }
}
