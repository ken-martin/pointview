#ifndef PointView_H
#define PointView_H

#include <QMainWindow>
#include <set>

// Forward Qt class declarations
class Ui_PointView;
class ppGenericTask;
class vtkPolyData;
class vtkPointGaussianMapper;
class vtkRenderRateCallback;
class vtkTimerLog;
class vtkCameraPass;
class vtkPointFillPass;
class vtkOpenGLRenderer;
class vtkLODPointCloudMapper;
class vtkActor;
class vtkUnsignedCharArray;

class PointView : public QMainWindow
{
  Q_OBJECT

public:

  // Constructor/Destructor
  PointView();
  ~PointView();

  virtual void UpdateFrameRate();

public slots:

  virtual void aboutToQuit();
  virtual void slotMaskPoints();
  virtual void slotOpenFile();
  virtual void slotConvertFile();
  virtual void slotSaveFile();
  virtual void slotNewDataPiece(int piece, vtkPolyData *);
  virtual void slotNewDataComplete();
  virtual void slotExit();
  virtual void slotTaskCompleted();
  virtual void slotUsePointFillPassChanged(int);
  virtual void slotCandidateRatioChanged(int);
  virtual void slotMinimumAngleChanged(int);
  virtual void scalarBoxClicked(bool);

protected:
  vtkRenderRateCallback *RenderRateCallback;
  vtkTimerLog *Timer;

protected slots:

private:
  // Designer form
  Ui_PointView *ui;
  std::set<ppGenericTask *> Tasks;

  vtkPointGaussianMapper *Mapper;
  vtkLODPointCloudMapper *LODMapper;
  vtkOpenGLRenderer *Renderer;
  vtkPolyData *CurrentData;
  vtkPointFillPass *PointFillPass;
  vtkCameraPass *CameraPass;
  vtkActor *Actor;

  float MemorySize;
  float NumberOfPoints;
  float PointMemory;
  float NumberOfCells;
  float CellMemory;
  float ScalarMemory;
};

#endif // PointView_H
