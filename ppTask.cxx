#include <QThread>
#include <QProgressBar>
#include <QLabel>
#include <QFormLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QStyle>
#include <QPushButton>

#include "ppTask.h"

ppTaskGUI::ppTaskGUI(QWidget *parent)
{
  this->Group = new QGroupBox(parent);
  this->Group->setFlat(true);

  QStyle *style = parent->style();
  const QIcon &icon = style->standardIcon(QStyle::SP_TitleBarCloseButton);
  this->CancelButton = new QPushButton(
    icon, "", this->Group);
  this->CancelButton->setFlat(true);
  this->CancelButton->setFixedSize(12,12);

  this->Bar = new QProgressBar(this->Group);
  this->Bar->setFormat("%p%");
  this->Bar->setRange(0,100);
  this->Bar->setFixedSize(160,12);

  this->Label = new QLabel(this->Group);

  QHBoxLayout *hbox = new QHBoxLayout();
  hbox->setContentsMargins(2,0,2,0);
  hbox->setSpacing(2);
  hbox->addWidget(this->CancelButton);
  hbox->addWidget(this->Bar);
  hbox->addWidget(this->Label);
  hbox->addStretch(1);
  this->Group->setLayout(hbox);

  this->CancelRequested = false;

  QObject::connect(
    this->CancelButton, SIGNAL(clicked()),
    this, SLOT(Cancel()),
    Qt::QueuedConnection);

  static_cast<QVBoxLayout*>(parent->layout())->addWidget(this->Group);
}

ppTaskGUI::~ppTaskGUI()
{
  delete this->Group;
  this->Group = 0;
}

void ppTaskGUI::Cancel()
{
  this->CancelRequested = true;
}

ppGenericTask::ppGenericTask(QWidget *parent)
{
  this->GUI = new ppTaskGUI(parent);
}

ppGenericTask::~ppGenericTask()
{
  delete this->GUI;
}

void ppGenericTask::SetLabel(const char *val)
{
  this->GUI->GetLabel()->setText(val);
}

QGroupBox *ppGenericTask::GetGroup()
{
  return this->GUI->GetGroup();
}

bool ppGenericTask::GetCancelRequested()
{
  return this->GUI->GetCancelRequested();
}

void ppGenericTask::SetCancelRequested(bool v)
{
  this->GUI->SetCancelRequested(v);
}
