#include "ppTaskThread.h"

#include "ppTask.h"

ppTaskThread::ppTaskThread()
{
  this->Task = NULL;
}

void ppTaskThread::SetTask(ppGenericTask *task)
{
  this->Task = task;
}

void ppTaskThread::UpdateProgress(int val)
{
  emit this->sendProgress(val);

  // check the CancelRequested
  if (this->Task->GetCancelRequested())
    {
    this->Cancel();
    }
}

void ppTaskThread::start()
{
  this->QThread::start();
}